/* Polyfills */
/* closest for IE11-IE8 */
(function () {
    'use strict';

    if(!Element.prototype.closest) {
        Element.prototype.closest = function (selector) {
            var elem = this;

            do {
                if (elem.matches(selector)) {
                    return elem;
                }

                elem = elem.parentElement;
            } while (elem);

            return null;
        };
    }
})();
/* matches for IE8 */
(function () {
    'use strict';

    if (document.body.matches === undefined) {
        Element.prototype.matches = function (selector) {
            var elemColl = this.parentNode.querySelectorAll(selector);

            for(var i = 0; i < elemColl.length; i++) {
                if (elemColl[i] === this) return true;
            }

            return false;
        }
    }
})();

/*Валидация формы*/
/*jQuery(document).ready(function ($) {
    /!*Валидация формы*!/
   // $(".editbutton").magnificPopup();
    $(".addinfof").magnificPopup();
    $(".addbutton").magnificPopup();

    $('body').on('submit', '#res-form', function (e) {
        var error = false, $this = $(this);
        $this.find('input:visible').each(function (index, element) {
            if ($(this).val() == '') {
                error = true;
                $(this).addClass('error');
            } else {
                $(this).removeClass('error');
            }
        });
        if ($this.find('textarea').val() == '') {
            error = true;
            $this.find('textarea').addClass('error');
        } else {
            $this.find('textarea').removeClass('error');
        }

        if (!error) {
            jQuery.ajax({
                'url': $this.attr('action'),
                'cache': false,
                'type': 'POST',
                'data': $this.serialize(),
                'success': function (html) {
                    $(".editbutton").magnificPopup('close');
                    $.magnificPopup.open({
                        items: {
                            src: '#yellowpopup'
                        },
                        type: 'inline'
                    });
                    $('#res-form')[0].reset();
                    yaCounter30552897.reachGoal('order');
                }
            });
        }
        return false;
    });

    $('body').on('change, keyup', "input[name='tel']", function (e) {
        var val = $(this).val();
        e = e || window.event;
        if (!(e.keyCode == '39' || e.keyCode == '37' || e.keyCode == '40' || e.keyCode == '38')) {
            val = $(this).val().replace(/[^0-9()/+-]+/g, '');
            if (val !== $(this).val())
                $(this).val(val);
        }
    });

    /!* Автозаполнение поиска *!/

    /!* поиск в шапке *!/
    $("#search input[name=q]").autocompvare({
        minLength: 3,
        source: function (request, response) {
            $.ajax({
                url: "/search/quick",
                data: $("#search").serialize(),
                success: function (data) {
                    data = JSON.parse(data);
                    response(data);
                }
            });
        },
        select: function (event, ui) {
            window.location.href = ui.item.alias;
        }
    });
    /!*поиск под признаками *!/
    $("#q").autocompvare({
        minLength: 3,
        source: function (request, response) {
            $.ajax({
                url: "/categories/autocompvare",
                data: $("#search2").serialize(),
                success: function (data) {
                    data = JSON.parse(data);
                    response(data);
                }
            });
        },
        select: function (event, ui) {
            window.location.href = ui.item.alias;
        }
    });

    /!*костыль для даты на странице событий*!/
    $('.schedule-place').each(function () {
        setSameSize($('.schedule-calendar', $(this).parent()), $(this), -10);
    });

    function setSameSize($source, $target, difference) {
        difference = difference || 0;

        if ($source.innerHeight() > $target.innerHeight()) {
            $target.innerHeight($source.innerHeight() + difference);
        }
    }
});*/

/* скрол вверх */
jQuery(document).ready(function ($) {
    var buttonUp = '<div id="scrollup"><i class="upButton fa fa-angle-up"></i></div>';
    $('body').append($(buttonUp));
    $('#scrollup').mouseover(function () {
        $(this).animate({opacity: 1}, 100);
    }).mouseout(function () {
        $(this).animate({opacity: 0.8}, 100);
    });
    $('#scrollup i').click(function () {
        $("html, body").animate({scrollTop: 0}, 500);
        return false;
    });
    var flag = false;
    $(window).scroll(function () {
        if ($(document).scrollTop() > $(window).height() && !flag) {
            $('#scrollup').fadeIn({queue: false, duration: 400});
            $('#scrollup').animate({'bottom': '10px'}, 400);
            flag = true;
        } else if ($(document).scrollTop() < $(window).height() && flag) {
            $('#scrollup').fadeOut({queue: false, duration: 400})
            $('#scrollup').animate({'bottom': '-20px'}, 400);
            flag = false;
        }
    });
});

/*share кнопки*/
/* вертикальный блок для картинок */
jQuery(document).ready(function ($) {
    var plusoBlockVertical = '<div class="pluso pluso-slider" data-background="transparent" data-options="medium,round,line,vertical,nocounter,theme=04" data-services="facebook,vkontakte" data-user="1485693136"></div>',
        plusoBlockHorizonthal = '<div class="pluso" data-background="none;" data-options="medium,square,line,horizontal,nocounter,sepcounter=1,theme=14" data-services="facebook,vkontakte,twitter" data-user="1485693136"></div>',
        plusoBlockHorizonthalComments = '<div class="pluso" data-background="none;" data-options="small,square,line,horizontal,nocounter,sepcounter=1,theme=14" data-services="vkontakte,facebook"></div>';
    $('.pluso-parent-v').prepend(plusoBlockVertical);
    $('.pluso-parent-h').prepend(plusoBlockHorizonthal);
    $('.share-comments > p:first-child').append(plusoBlockHorizonthalComments);

    if (window.pluso)if (typeof window.pluso.start == "function") return;
    if (window.ifpluso == undefined) {
        window.ifpluso = 1;
        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript';
        s.charset = 'UTF-8';
        s.async = true;
        s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
        var h = d[g]('body')[0];
        h.appendChild(s);

        s.onload = function () {
            var divArr = '.pluso-parent-h span.pluso-wrap > div';

            checkForElements(divArr, function (elements) {
                var plusoA = '.pluso-parent-h span.pluso-wrap > a',
                    plusoWrap = '.pluso-parent-h span.pluso-wrap';

                addNames(elements);
                $(plusoA).css('vertical-align', 'bottom');
                $(plusoWrap).css('padding-bottom', '20px');
            });
        };
    }

    function checkForElements(elements, callback) {
        var timer = setInterval(function () {
            if ($(elements).length) {
                clearInterval(timer);
                callback(elements);
            }
        }, 20);
    }

    function addNames(arr) {
        $(arr).each(function () {
            var $a = $('a', this),
                bgColor = $(this).css('background-color'),
                $span = $('<span></span>');

            $span.text($a.attr('title'));
            $span.css('background-color', bgColor);
            $span.addClass('share-text');
            $a.css('width', 'auto');

            $a.append($span);

            delegateClick($span, $a);
        });
    }

    function delegateClick($from, $to) {
        $from.on('click', function (e) {
            var link = $to[0];
            var linkEvent = null;
            if (document.createEvent) {
                linkEvent = document.createEvent('MouseEvents');
                linkEvent.initEvent('click', true, true);// для всех остальных кроме IE
                link.dispatchEvent(linkEvent); //для Safari
            }
            else if (document.createEventObject) { //для обделенного любовью IE
                linkEvent = document.createEventObject();
                link.fireEvent('onclick', linkEvent);
            }

            e.preventDefault();
        });
    }
});

/*menu plugin*/
(function ($) {
    $(document).ready(function () {
        var collaborate = {
            activeTab: null
        };

        function TabMenu() {
            this.defaults = {
                openMenuClass: '.tab-open',
                openMenuActiveClass: '.tab-active',
                closeMenuClass: '.tab-close',
                panelMenuClass: '.tab-panel',
                contentBlockBtnClass: '.tab-content-block-btn',
                contentBlockBtnActiveClass: '.tab-active',
                contentBlockClass: '.tab-content-block',
                choosenBlockClass: '.tab-choosen',
                activeClass: '.tab-active', //не используется, оставил на всякий случай
                contentListClass: '.tab-content-list'
            };
            this.initialized = {
                $menu: null,
                $openBtn: null,
                $closeBtn: null,
                $menuPanel: null,
                $contentBlockBtn: null,
                $contentList: null,
                isChanging: false
            };
            this.options = {};
            this.init = function ($menu, userOptions) {
                $.extend(this.options, this.defaults, userOptions);

                var self = this;
                var sIn = self.initialized;
                var sOp = self.options;


                sIn.$openBtn = $(sOp.openMenuClass, $menu);
                sIn.$closeBtn = $(sOp.closeMenuClass, $menu);
                sIn.$menuPanel = $(sOp.panelMenuClass, $menu);
                sIn.$contentBlockBtn = $(sOp.contentBlockBtnClass, $menu);
                sIn.$contentList = $(sOp.contentListClass + '> li', sIn.$contentBlockBtn);
                sIn.$menu = $menu;


                sIn.$openBtn.click(function () {
                    if($(this).hasClass(sOp.openMenuActiveClass.slice(1))){
                        self.menuClose();
                        collaborate.activeTab = null;
                    } else {
                        if(collaborate.activeTab && collaborate.activeTab != self) {
                            collaborate.activeTab.menuClose();
                        }
                        self.menuOpen();
                        collaborate.activeTab = self;
                    }
                });
                sIn.$closeBtn.click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    self.menuClose();
                    collaborate.activeTab = null;
                });
                sIn.$contentBlockBtn.bind({
                    'mouseenter': function (e) {
                        e.stopPropagation();

                        self.showBlock($(this));
                    },
                    'click': function (e) {
                        e.stopPropagation();

                        self.chooseBlock($(this));
                    }
                });
                sIn.$contentList.click(function (e) {
                    e.stopPropagation();

                    self.menuClose();
                    self.chooseBlock($(this).closest(sIn.$contentBlockBtn));
                    activeTab = null;
                });


            };
            this.menuOpen = function () {
                var self = this;

                self.initialized.$menuPanel.slideDown('fast', 'linear', function () { //easeOutQuad
                    self.initialized.$openBtn.addClass(self.options.openMenuActiveClass.slice(1));
                });
            };
            this.menuClose = function () {
                var self = this;

                self.initialized.$menuPanel.slideUp('fast', 'linear', function () {
                    self.initialized.$openBtn.removeClass(self.options.openMenuActiveClass.slice(1));
                });
            };
            this.showBlock = function ($blockBtn) {
                var self = this;
                var active = self.options.contentBlockBtnActiveClass.slice(1);

                if( !$blockBtn.hasClass(active) && !self.initialized.isChanging ){
                    self.initialized.isChanging = true;

                    $blockBtn.addClass(active);
                    $blockBtn.children(self.options.contentBlockClass).fadeIn('normal', 'swing', function () {
                        self.initialized.$contentBlockBtn.not($blockBtn).each(function () {
                            $(this).removeClass(active);
                            $(this).children(self.options.contentBlockClass).fadeOut('normal', 'swing', function () {
                                self.initialized.isChanging = false;
                            });
                        });
                    });
                }
            };
            this.chooseBlock = function ($block) {
                var self = this;
                var sIn = self.initialized;
                var sOp = self.options;
                var choosen = sOp.choosenBlockClass.slice(1);

                $block.toggleClass(choosen);

                if($block.hasClass(choosen)) {
                    sIn.$contentBlockBtn.unbind('mouseenter');
                    sIn.$contentBlockBtn.not($block).each(function () {
                        $(this).removeClass(choosen);
                    });
                    self.showBlock($block);
                } else {
                    sIn.$contentBlockBtn.bind({
                        'mouseenter': function (e) {
                            e.stopPropagation();

                            self.showBlock($(this));
                        }
                    });
                }
            };
        }

        $.fn.tabMenu = function (userOptions) {
            return $(this).each(function () {
                var tMenuCopy = new TabMenu();

                tMenuCopy.init($(this), userOptions);
                $(this).data('collaborate', collaborate);
            });
        };
    });
})(jQuery);
/*Menu continent*/
jQuery(document).ready(function ($) {
    var collaborate = $('#tab-menu').tabMenu({
        openMenuActiveClass: '.active-tab',
        contentBlockBtnActiveClass: '.active-continent'
    }).data('collaborate');

    $('#tab-menu-city').tabMenu({
        openMenuActiveClass: '.active-tab',
        contentBlockBtnActiveClass: '.active-continent'
    });

    $('#tab-menu-theme').tabMenu({
        openMenuActiveClass: '.active-tab',
        contentBlockBtnActiveClass: '.active-continent'
    });

    function SimpleTab(options) {
        this._$openSearch = options.$openSearch;
        this._$searchBtn = options.$searchBtn;
        this._collaborate = options.collaborate;
    }
    SimpleTab.prototype.run = function (e) {
        var $target = $(e.target);

        if($target.is(this._$searchBtn)){
            if(this._collaborate.activeTab && this._collaborate.activeTab != this) {
                this._collaborate.activeTab.menuClose();
            }
            this.menuOpen();
            this._collaborate.activeTab = this;
        } else if(!$target.parents('.open-search').length && this._$openSearch.hasClass('active')) {
            this.menuClose();
            this._collaborate.activeTab = null;
        }
    };
    SimpleTab.prototype.menuOpen = function () {
        this._$openSearch.toggleClass('active');
    };
    SimpleTab.prototype.menuClose = function () {
        this._$openSearch.removeClass('active');
    };

    var searchMenu = new SimpleTab({
        collaborate: collaborate,
        $openSearch: $('.open-search'),
        $searchBtn: $('.fa.fa-search')
    });

    $('body').click(function (e) {
        searchMenu.run(e);
    });


   /* $('.top').click(function (e) {
        var $target = $(e.target);
        data-menu="tab-menu"
    })*/
});

//Comments
jQuery(document).ready(function ($) {
    var li = document.querySelectorAll('.stars-evaluation');
    var evaluate = function (e) {
        e.stopPropagation();
        var target = e.target;
        var firstStar = target.parentElement.lastElementChild;

        Array.prototype.forEach.call(target.parentElement.children, function (item) {
            item.classList.remove('evaluated');
        });

        while(1){
            target.classList.add('evaluated');
            if(target == firstStar) break;

            target = target.nextElementSibling;
        }

    };

    Array.prototype.forEach.call(li, function (item) {
        item.addEventListener('click', evaluate, false);
    });
});

//Widget
jQuery(document).ready(function ($) {
    /*Widget*/
    function SelectWidget(options) {
        this._widget = options.widget;
        this._listenedBlockSelector = options.listenedBlockSelector || 'body';
        this._removeBtn = '<a class="close" data-action="remove-active-element" href="#"></a>';
        this._CLASSES = {
            active: 'active'
        };
    }
    SelectWidget.prototype.init = function () {
        this._activeList = this._widget.querySelector('[data-component="active-list"]');
        this._inactiveList = this._widget.querySelector('[data-component="inactive-list"]');
        this.saveOriginWidget();

        document.querySelector(this._listenedBlockSelector).addEventListener('click', this.widgetListener.bind(this));
    };
    SelectWidget.prototype.widgetListener = function (e) {
        var target = e.target;
        var widget = target.closest('[data-component="widget"]');

        if (!widget && !this._widget.classList.contains(this._CLASSES.active)) return;

        if (!widget && this._widget.classList.contains(this._CLASSES.active)) {
            this._widget.classList.remove(this._CLASSES.active);
            return;
        }

        if (!widget.classList.contains(this._CLASSES.active)) {
            widget.classList.add(this._CLASSES.active);
        }

        if (this.isRemoveBtn(target)) {
            this.moveElemToInactive(target.closest('li'));
        }

        if (this.isInactiveList(target)) {
            this.moveElemToActive(target.closest('li'));
        }

        e.preventDefault();
    };
    SelectWidget.prototype.isInactiveList = function (elem) {
        var li = elem.closest('li');
        if (!li) return false;

        return this._inactiveList.contains(li);
    };
    SelectWidget.prototype.isRemoveBtn = function (elem) {
        return !!elem.closest('[data-action="remove-active-element"]');
    };
    SelectWidget.prototype.moveElemToActive = function (inactiveElem) {
        var activeElem = document.createElement('li');
        var span = document.createElement('span');
        var activeList = inactiveElem.closest('[data-component="widget"]').querySelector('[data-component="active-list"]');

        span.setAttribute('data-component', 'content');
        span.setAttribute('data-name', 'category_' + activeList.children.length);
        span.textContent = inactiveElem.textContent;
        activeElem.innerHTML = this._removeBtn;
        activeElem.insertBefore(span, activeElem.firstElementChild);

        activeList.appendChild(activeElem);
        inactiveElem.parentNode.removeChild(inactiveElem);
    };
    SelectWidget.prototype.moveElemToInactive = function (activeElem) {
        var inactiveElem = document.createElement('li');
        var inactiveList = activeElem.closest('[data-component="widget"]').querySelector('[data-component="inactive-list"]');

        inactiveElem.textContent = activeElem.querySelector('[data-component="content"]').textContent;

        inactiveList.appendChild(inactiveElem);
        activeElem.parentNode.removeChild(activeElem);
    };
    SelectWidget.prototype.saveOriginWidget = function () {
        this._originActiveList = this._activeList.cloneNode(true);
        this._originInactiveList = this._inactiveList.cloneNode(true);
    };
    SelectWidget.prototype.resetWidget = function () {
        this._activeList.innerHTML = this._originActiveList.innerHTML;
        this._inactiveList.innerHTML = this._originInactiveList.innerHTML;
    };

    var categoryWidget = new SelectWidget({
        widget: document.querySelector('[data-component="widget"]')
    });
    categoryWidget.init();
});

/*Forms*/
jQuery(document).ready(function ($) {
    /* form control class*/
    function FormController(options) {
        this._submitSelector = options.submitSelector || 'input[type="submit"]';
        this._listenedBlock = options.listenedBlock || 'body';
        this._resetForm = options.resetForm || true;
        this._beforeSend = options.beforeSend || null;
        this._resolve = options.resolve || null;
        this._reject = options.reject || null;
        this._maxFileSize = options.maxFileSize || 2; //MB
    }
    FormController.prototype.init = function () {
        if(!document.querySelector(this._submitSelector)) return;

        $(this._listenedBlock).click(this.formListeners.bind(this));

        if($(this._listenedBlock).find('input[type="file"]').length) {
            $(this._listenedBlock).change(this.uploadListener.bind(this));
        }
    };
    FormController.prototype.validateForm = function (form) {
        var vResult = true;
        var passCurr = false;
        var self = this;

        $('input[name!="submit"], textarea', $(form)).each(function () {
            var vVal = $(this).val();
            var requiredField = $(this).attr('required');
            var pattern = '';
            var placeholderMess = '';

            $(this).removeClass('form-fail'); //чистим классы, если остались после прошлого раза
            $(this).removeClass('form-success');


            if (vVal.length === 0 && requiredField) {
                placeholderMess = 'Заполните ' + ($(this).attr('data-validate-empty') ? $(this).attr('data-validate-empty') : 'поле') + '!';
                vResult = false;
            } else if ($(this).attr('name') == 'email' && vVal.length) {
                pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

                if (pattern.test($(this).val())) {
                    $(this).addClass('form-success');
                } else {
                    placeholderMess = 'Введите корректный E-mail!';
                    vResult = false;
                }
            } else if ($(this).attr('name') == 'phone' && vVal.length) {
                pattern = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/i;

                if (pattern.test($(this).val())) {
                    $(this).addClass('form-success');
                } else {
                    placeholderMess = 'Введите корректный телефон!';
                    vResult = false;
                }
            } else if ($(this).attr('name') === 'passCurr' && vVal.length) {
                passCurr = this;
            } else if ($(this).attr('name') === 'passNew' && vVal.length) {
                if (vVal === $(passCurr).val()) {
                    $(passCurr).val('').addClass('form-fail').attr('placeholder', 'Новый пароль, не должен совпадать с текущим!');
                    placeholderMess = 'Новый пароль, не должен совпадать с текущим!';
                } else {
                    $(this).addClass('form-success');
                    $(passCurr).addClass('form-success');
                }
            }else if($(this).is('textarea') && vVal.length < 10 && vVal.length > 0  && requiredField) {
                placeholderMess = 'Вопрос слишком короткий!';
                vResult = false;
            } else if (requiredField && vVal.length) {
                $(this).addClass('form-success');
            }

            if (placeholderMess) {
                $(this).attr('data-old-placeholder', $(this).attr('placeholder'));
                $(this).val('').attr('placeholder', placeholderMess).addClass('form-fail');
                placeholderMess = '<span class="form-fail">' + placeholderMess + '</span>';
                self.changeLabel(this, placeholderMess, 'span.placeholder');
            }
        });

        return vResult;
    };
    FormController.prototype.uploadListener = function (e) {
        var elem = e.target;

        if(!elem.matches('input[type="file"]'))  return;

        var size = this.getFileSize(elem);

        if (size < this._maxFileSize * 1024 * 1024) return;

        alert("Файл слишком большой. Размер вашего файла " + (size / 1024 / 1024).toFixed(2) +
            " MB. Загружайте файлы меньше " + this._maxFileSize + "MB.");
        $(elem).val('');
    };
    FormController.prototype.getFileSize = function (input) {
        var file;

        if (typeof ActiveXObject == "function") { // IE
            file = (new ActiveXObject("Scripting.FileSystemObject")).getFile(input.value);
        } else {
            file = input.files[0];
        }

        return file.size;
    };
    FormController.prototype.changeLabel = function (elem, val, insideLabelSelector) {
        var selector = 'label[for="' + $(elem).attr('id') + '"] ' + insideLabelSelector || '';
        var $label = $(selector);

        if ($label.length) {
            $label.each(function () {
                this.innerHTML = val;
            });
        }
    };
    FormController.prototype.resetForms = function (formContainer) {
        var $form;
        var self = this;

        if (formContainer.tagName === 'FORM') {
            $form = $(formContainer);
        } else {
            $form = $('form', $(formContainer));
        }

        $form.each(function () {
            self.resetPlaceholders(this);
            if (self._resetForm) {
                this.reset();
                self.triggerChange(this);
            }
        });
    };
    FormController.prototype.resetPlaceholders = function (inputContainer) {
        var self = this;
        var $input;

        if (inputContainer.tagName === 'INPUT') {
            $input = $(inputContainer);
        } else {
            $input = $('input[name != submit]', $(inputContainer));
        }

        $input.each(function () {
            var name = $(this).attr('name');
            var placeholderMess =  $(this).attr('data-old-placeholder');

            $(this).removeClass('form-success');
            $(this).removeClass('form-fail');

            if (!placeholderMess) return;

            $(this).attr('placeholder', placeholderMess);
            self.changeLabel(this, placeholderMess, 'span.placeholder');
        });
    };
    FormController.prototype.triggerChange = function (inputContainer) {
        var $input = null;

        if (inputContainer.tagName === 'INPUT') {
            $input = $(inputContainer);
        } else {
            $input = $('input[name != submit]', $(inputContainer));
        }

        $input.each(function () {
            $(this).trigger('change');
        });
    };
    FormController.prototype.formListeners = function (e) {
        var elem = e.target;

        if (!elem.matches(this._submitSelector)) return;

        e.preventDefault();

        var form = elem.closest('form');

        if (this.validateForm(form)) {
            this.sendRequest(form, this._resolve, this._reject, this._beforeSend);
        }
    };
    FormController.prototype.sendRequest = function (form, resolve, reject, beforeSend) {
        var formData = $(form).serializeArray(); //собираем все данные из формы
        var self = this;


        if (beforeSend) {
            beforeSend.call(this, formData, form);
        }
        //console.dir(formData);

        this.showPending(form);

        $.ajax({
            type: form.method,
            url: form.action,
            data: $.param(formData),
            success: function (response) {
                //console.log(response);

                if (response) {
                    self.hidePending(form, self.showSuccess.bind(self, form));

                    if (resolve) {
                        resolve.call(self, form, response);
                    }
                } else {
                    self.hidePending(form, self.showError.bind(self, form));

                    if (reject) {
                        reject.call(self, form, response);
                    }
                }

                self.resetForms(form);
            },
            error: function (response) {

                //console.log(response);
                //throw new Error(response.statusText);
                self.hidePending(form, self.showError.bind(self, form));
                self.resetForms(form);

            }
        });
    };
    FormController.prototype.showError = function (form) {
        var $errBlock = $('.err-block', $(form));

        $('.form-success', $(form)).removeClass('form-success');
        $errBlock.fadeIn('normal');

        setTimeout(function () {
            $errBlock.fadeOut('normal');
        }, 10000);
    };
    FormController.prototype.showSuccess = function (form) {
        var $succBlock = $('.succ-block', $(form));

        $('.form-success', $(form)).removeClass('form-success');
        $succBlock.fadeIn('normal');

        setTimeout(function () {
            $succBlock.fadeOut('normal');
        }, 10000);
    };
    FormController.prototype.showPending = function (form) {
        var $pendingBlock = $('.pend-block', $(form));

        $pendingBlock.fadeIn('normal');
    };
    FormController.prototype.hidePending = function (form, callback) {
        var $pendingBlock = $('.pend-block', $(form));

        if (!$pendingBlock[0]) {
            callback();
            return;
        }

        $pendingBlock.fadeOut('normal', 'linear', callback);
    };

    var expandedCommentsForm = new FormController({
        submitSelector: '.send-comment'
    });
    expandedCommentsForm.init();
});

/*Tooltip*/
jQuery(document).ready(function ($) {
    function TooltipConstructor (options) {
        this._listenedBlock = options.listenedBlock || document.body;
        this._classArr = options.tooltipClass ? options.tooltipClass.split(', ') : null;
    }
    TooltipConstructor.prototype.init = function () {
        var self = this;

        $(this._listenedBlock).each(function () {
            $(this).on({
                'mouseover': self.renderTooltip.bind(self),
                'mouseout': self.removeTooltip.bind(self)
            });
        });
    };
    TooltipConstructor.prototype.renderTooltip = function (e) {
        var target = e.target;
        target = target.closest('[data-tooltip]');

        if(!target) return;

        var tooltipText = target.getAttribute('data-tooltip');
        var tooltip = document.createElement('span');
        tooltip.id = 'tooltip';
        if (this._classArr) {
            tooltip.classList.add.apply(tooltip.classList, this._classArr);
        }
        //tooltip.className = this._tooltipClasses;
        tooltip.style.position = 'fixed';
        tooltip.innerHTML = tooltipText;
        document.body.appendChild(tooltip);

        var coordsBtn = target.getBoundingClientRect();
        var coordsTooltip = tooltip.getBoundingClientRect();
        var extraMargin = 5;

        var tooltipWidth = coordsTooltip.right - coordsTooltip.left;
        var tooltipHeight = (coordsTooltip.bottom - coordsTooltip.top) + extraMargin;
        var btnWidth = coordsBtn.right - coordsBtn.left ;
        var toolPosX = coordsBtn.left + (btnWidth - tooltipWidth) / 2;

        tooltip.style.left = (toolPosX >= 0 ? toolPosX : 0) + 'px';
        tooltip.style.top = (document.documentElement.clientHeight - coordsBtn.bottom  < tooltipHeight ? coordsBtn.top - tooltipHeight : coordsBtn.bottom + extraMargin) + 'px';
    };
    TooltipConstructor.prototype.removeTooltip = function() {
        var tooltip = document.getElementById('tooltip');

        if(!tooltip) return;

        document.body.removeChild(tooltip);
    };


var editorTooltip = new TooltipConstructor({
    listenedBlock: document.querySelectorAll('.redactor-toolbar'),
    tooltipClass: 'support'
});
    editorTooltip.init();

});



