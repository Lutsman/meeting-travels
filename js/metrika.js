/*Google analytics*/

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-243276-1', 'auto');
ga('send', 'pageview');


/* Баннеры Google*/

(function addAdsBanners(win,doc){
    if (doc.getElementsByClassName("adsbygoogle").length){
        var n = doc.getElementsByTagName("script")[0],
            s = doc.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.async = true;
        s.src = "//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js";

        if (win.opera == "[object Opera]") {
            doc.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }

        for(var i=0; i<doc.getElementsByClassName("adsbygoogle").length; i++){
            (adsbygoogle = win.adsbygoogle || []).push({});
        }
    }
})(window, document);


/*Карта Google*/

function initialize() {

    var markers = [];
    var canvas=document.getElementById('map-canvas');
    var myLatlng = new google.maps.LatLng(canvas.getAttribute("data-lat"),canvas.getAttribute("data-long"));
    var map = new google.maps.Map(canvas, {
        //mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoom: parseInt(canvas.getAttribute("data-zoom")),
        center: myLatlng,
        streetViewControl: false,
        scaleControl: false,
        panControl: false,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.BIG
        }

    });

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: canvas.getAttribute("data-name"),
        draggable: false
    });
}

if (document.getElementById('map-canvas')){
    google.maps.event.addDomListener(window, 'load', initialize);
}




/*Яндекс метрика*/
/*

(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter54680 = new Ya.Metrika({
                id:54680,
                webvisor:true,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true
            });
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");

*/


